require 'spec_helper'
require_relative '../../lib/models/address_book.rb'

describe AddressBook do

  context 'attributes' do
    it "should respond to entries" do
      book = AddressBook.new
      expect(book).to respond_to(:entries)
    end

    it 'should intialize entries as an array' do
      book = AddressBook.new
      expect(book.entries).to be_a(Array)
    end

    it 'should intialize entries as empty' do
      book = AddressBook.new
      expect(book.entries.size).to eq 0
    end

  end # attributes

  context ".add_entry" do

    it 'add only one entry to the address book' do
      book = AddressBook.new
      book.add_entry('Jose Rosado', '809-555-5555', 'jose@rosa.do')

      expect(book.entries.size).to eq 1
    end

    it 'adds the correct information to entries' do
      book = AddressBook.new
      book.add_entry("Jose Rosado", '809-555-5555', 'jose@rosa.do')
      new_entry = book.entries[0]

      expect(new_entry.name).to eq 'Jose Rosado'
      expect(new_entry.phone_number).to eq '809-555-5555'
      expect(new_entry.email).to eq 'jose@rosa.do'
    end

  end # .add_entry

  context ".remove_entry" do

    it "deletes one entry from the address book" do
      book = AddressBook.new
      book.add_entry('Jose', '809', 'jose@rosado')
      book.add_entry('Ricardo', '829', 'admin@mail.com')
      deleted = book.remove_entry('Jose', '809', 'jose@rosado')

      expect(book.entries.count).to eq 1
    end

  end


end