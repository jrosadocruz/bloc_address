require 'spec_helper'
require_relative '../../lib/models/entry.rb'

describe Entry do

  context "attributes" do

    it "should respond to name" do
      entry = Entry.new('Jose Rosado', '809555-5555', 'jose@rosa.do')
      expect(entry).to respond_to(:name)
    end

    it "should respond to phone number" do
      entry = Entry.new('Jose Rosado', '809555-5555', 'jose@rosa.do')
      expect(entry).to respond_to(:phone_number)
    end

    it "should respond to email" do
      entry = Entry.new('Jose Rosado', '809555-5555', 'jose@rosa.do')
      expect(entry).to respond_to(:email)
    end

  end

  context ".to_s" do

    it "prints an entry as a string" do
      entry = Entry.new('Jose Rosado', '809555-5555', 'jose@rosa.do')
      expected_string = "Name: Jose Rosado\nPhone Number: 809555-5555\nEmail: jose@rosa.do"

      expect(entry.to_s).to eq(expected_string)
    end

  end

end