require_relative 'entry.rb'
class AddressBook
  attr_accessor :entries

  def initialize
    @entries = []
  end

  def add_entry name, phone, email
    index = 0
    @entries.each { |entry| break if name < entry.name ; index += 1 }
    @entries.insert(index, Entry.new(name, phone, email))
  end

  def remove_entry name, phone, email
    index = 0
    @entries.each do |entry|
      break if name == entry.name && phone == entry.phone_number && email == entry.email
      index += 1
    end
    @entries.delete_at(index)
  end

end